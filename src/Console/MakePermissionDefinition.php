<?php

namespace mathewparet\LaravelPermissionSeeder\Console;

use Illuminate\Support\Str;
use InvalidArgumentException;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

class MakePermissionDefinition extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:permission-seeder {model} {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a permission seeder with permissions';

    protected $type = 'PermissionSeeder';

    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);

        $modelClass = $this->parseModel($this->argument('model'));

        $search = [
            'DummyPermissionSeeder',
            '{{ model }}',
            '{{ Model }}',
        ];

        $replacements = [
            $this->argument('name'),
            Str::headline(class_basename($modelClass)),
            class_basename($modelClass),
        ];

        return str_replace($search, $replacements, $stub);
    }

    protected function qualifyClass($name)
    {
        $name = ltrim($name, '\\/');

        $name = str_replace('/', '\\', $name);

        return $name;
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/DummyPermissionSeeder.stub';
    }

    protected function getPath($name)
    {
        return $this->laravel->databasePath().'/seeders/'.$name.'.php';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $name
     * @return string
     */
    protected function getNamespace($name)
    {
        return 'Database\Seeders';
    }

    /**
     * Get the fully-qualified model class name.
     *
     * @param  string  $model
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    protected function parseModel($model)
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $model)) {
            throw new InvalidArgumentException('Model name contains invalid characters.');
        }

        return $this->qualifyModel($model);
    }
}
