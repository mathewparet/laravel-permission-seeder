<?php
namespace mathewparet\LaravelPermissionSeeder\Seeder;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Permission;

abstract class RolesAndPermissions extends Seeder
{
    protected $connection = null;

    protected $permissions = [
        
    ];

    protected $roles = [

    ];

    protected $guard_name = 'web';

    public function connectionName()
    {
        return $this->connection;
    }

    private static function getConnection()
    {
        return (new static)->connectionName();
    }

    /**
     * Seed permissions
     */
    public static function seed()
    {
        $parameters = [
            '--class' => class_basename(static::class),
            '--force' => true
        ];

        if(static::getConnection()) {
            $parameters['--database'] = static::getConnection();
        }

        Artisan::call('db:seed', $parameters);
    }

    /**
     * Unseed or remove permissions
     */
    public static function unseed()
    {
        (new static)->rollback();
    }

    public function rollback()
    {
        $this->deletePermissions();
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->createPermissions();
    }

    private function getGuardName()
    {
        return $this->guard_name ?? config('auth.defaults.guard');
    }

    private function deletePermissions()
    {
        collect($this->permissions)->each(function($permission) {
            Permission::findByName($permission, $this->getGuardName())->delete();
        });
    }


    private function createPermissions()
    {
        collect($this->permissions)->each(function($permission) {
            Permission::findOrCreate($permission, $this->getGuardName());
        });
    }

}