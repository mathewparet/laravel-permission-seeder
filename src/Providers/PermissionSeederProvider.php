<?php
namespace mathewparet\LaravelPermissionSeeder\Providers;

use Illuminate\Support\ServiceProvider;
use mathewparet\LaravelPermissionSeeder\Console\MakePermissionDefinition;

class PermissionSeederProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('command.make:permission-seeder', MakePermissionDefinition::class);

        $this->commands([
            'command.make:permission-seeder'
        ]);
    }
}