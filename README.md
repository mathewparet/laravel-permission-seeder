# mathewparet/laravel-permission-seeder

A seeder helper to create and remove permissions.

# Installation

```sh
composer require mathewparet/laravel-permission-seeder
```

# Usage

To create a permission seeder:

```sh
php artisan make:permission-seeder User UserPermissionSeeder
```

To create permissions along with migration, add this to the `up()` function in the migration:

```php
UserPermissionSeeder::seed();
```

Similarly, to remove permissions while rolling back a migration, add this to the `down()` function in the migration:

```php
UserPermissionSeeder::unseed();
```